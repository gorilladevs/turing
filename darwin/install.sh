#!/bin/bash

main() {
  if which tput >/dev/null 2>&1; then
      ncolors=$(tput colors)
  fi
  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
    UNDERLINE="$(tput sgr 0 1)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
    UNDERLINE=""
  fi

  set -e

  printf "${BLUE}\n\n"
  printf "${GREEN}${BOLD}"
  printf "    ${BLUE}${BOLD}** ********** *******    ******** \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**${NORMAL}/////${BLUE}${BOLD}**${NORMAL}/// /${BLUE}${BOLD}**${NORMAL}////${BLUE}${BOLD}**  **${NORMAL}//////  \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**   ${NORMAL}/${BLUE}${BOLD}** ${NORMAL}/${BLUE}${BOLD}**        \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}*******  ${NORMAL}/${BLUE}${BOLD}********* \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**${NORMAL}///${BLUE}${BOLD}**  ${NORMAL}////////${BLUE}${BOLD}** \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**  ${NORMAL}//${BLUE}${BOLD}**        ${NORMAL}/${BLUE}${BOLD}** \n"
  printf "   ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**    ${NORMAL}/${BLUE}${BOLD}**   ${NORMAL}//${BLUE}${BOLD}** ********  \n"
  printf "   ${NORMAL}//     //     //     // ////////   \n"
  printf "   ----------------------------------\n"
  printf "     Resource Center Installation\n"
  printf "${NORMAL}${BLUE}\n"
  echo '------------------------------------------------------------------'
  printf "${NORMAL}\n\n"

  if [ -d "/sites" ]; then
    printf "${RED}Sites instance found!${NORMAL}\n"
    exit
  else
    printf "${YELLOW}Creating /sites home...${NORMAL}\n"
    mkdir /sites
  fi

  printf "${GREEN}Preparing Darwin Agent Installation${NORMAL}\n"
  printf "${NORMAL}${BLUE}\n"
  echo '------------------------------------------------------------------'

  printf "${GREEN}Checking if installed already... ${NORMAL}"

  if [ -d "/sites/darwin" ]; then
    printf "${RED}Darwin Agent is already installed!${NORMAL}\n"
    exit
  else
    printf "${YELLOW}not installed!${NORMAL}\n"
  fi

  if [ ! -n "$DARWIN" ]; then
    DARWIN=/sites/darwin
  fi

  umask g-w,o-w
  printf "${GREEN}Checking required tools..."
  hash git >/dev/null 2>&1 || {
    printf "${RED}Failed. Git is not installed.${NORMAL}\n"
    exit 1
  }
  printf "${YELLOW}done.${NORMAL}\n"

  printf "${YELLO}Git Credentials Required:\n"
  read -s -p "Please enter your Git Username: " git_user
  read -s -p ", then your password: " git_password

  printf "${GREEN}Thanks! ${BLUE}${BOLD}$git_user${NORMAL}\n\n"

  printf "${GREEN}Cloning Darwin...${YELLOW}\n"
  env git clone --depth=1 https://$git_user:$git_password@gitlab.com/gorilladevs/darwin.git $DARWIN || {
    printf "${RED}Failed to clone Darwin source repository.${NORMAL}\n"
    exit 1
  }

  printf "${GREEN}Creating defaults...${NORMAL}\n"
  cd $DARWIN
  mkdir output
  mkdir transfers

  printf "${GREEN}Installing dependencies...${NORMAL}\n"
  npm install

  printf "${GREEN}"
  echo 'Darwin Agent Installation Completed.'
  printf "${NORMAL}${BLUE}\n\n"

  printf "${GREEN}\nPreparing Site Instance Installation"
  printf "${NORMAL}${BLUE}\n"
  echo '------------------------------------------------------------------'
  cp -r tools/sites/ /sites
  mkdir /sites/backups
  mkdir /sites/log
  mkdir /sites/docs
  mkdir /sites/docs/conf

  printf "${GREEN}"
  echo 'Site Preparation Completed.'
  printf "${NORMAL}${BLUE}\n\n"

  printf "${GREEN}\nPreparing Liberty Services Installation"
  printf "${NORMAL}${BLUE}\n"
  echo '------------------------------------------------------------------'

  if [ ! -n "$LIBERTY" ]; then
    LIBERTY=/sites/liberty
  fi

  printf "${GREEN}Cloning Liberty Services...${YELLOW}\n"
  env git clone --depth=1 https://$git_user:$git_password@gitlab.com/guerilladevs/liberty.git /sites/liberty_tmp || {
    printf "${RED}Failed to clone Liberty source repository.${NORMAL}\n"
    exit 1
  }

  printf "${GREEN}Setting up Liberty Services...${NORMAL}\n"
  mkdir /sites/liberty
  cd /sites/liberty_tmp
  cp *.config /sites/liberty
  cp package.json /sites/liberty
  cp server.js /sites/liberty
  cp -rf apis /sites/liberty

  cd /sites/liberty
  rm -rf /sites/liberty_tmp

  printf "${GREEN}Installing dependencies...${NORMAL}\n"
  npm install

  printf "${GREEN}"
  echo 'Liberty Services Installation Completed.'
  printf "${NORMAL}${BLUE}\n\n"

  echo '------------------------------------------------------------------'
  printf "${BOLD}${BLUE}${UNDERLINE}Liberty Services: ${NORMAL}\n"
  printf "${BOLD}${YELLOW}To configure: ${NORMAL}\n"
  printf "    For ${BOLD}Atlassian Crowd Settings${NORMAL}, edit atllasian.config\n"
  printf "    For ${BOLD}Bintray Settings${NORMAL}, edit bintray.config\n"
  printf "\n${BOLD}${YELLOW}To start: ${NORMAL}\n"
  echo '     Go to /sites/liberty folder then issue this command '
  echo '    pm2 start server.js -i 0 --name "liberty_services"'
  printf "${NORMAL}${BLUE}\n\n"

  printf "${BOLD}${BLUE}${UNDERLINE}Site Web Service: ${NORMAL}\n"
  printf "\n${BOLD}${YELLOW}To start: ${NORMAL}\n"
  printf "     Start ${BOLD}NGINX${NORMAL} service"
  printf "${NORMAL}${BLUE}\n\n"

  printf "${BOLD}${BLUE}${UNDERLINE}Darwin Deploy Agent: ${NORMAL}\n"
  printf "${BOLD}${YELLOW}To configure: ${NORMAL}\n"
  echo '    Edit darwin.config and set all necessary folder information.'
  printf "\n${BOLD}${YELLOW}To start: ${NORMAL}\n"
  echo '    Go to /sites/darwin folder then issue this command '
  echo '    pm2 start agent.js -i 0 --name "darwin_agent"'
  printf "${NORMAL}${BLUE}"
  echo '------------------------------------------------------------------'
  printf "${NORMAL}"

}

main
